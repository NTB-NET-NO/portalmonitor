Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports System.Text

Imports System.Net
Imports System.Web.Mail

Public Class ConfigForm
    Inherits System.Windows.Forms.Form

    'Variables - Options
    Private silentMode As Boolean = False
    Private loggingMode As Boolean = False
    Private emailMode As Boolean = False

    Private timeout As Integer
    Private logFile As String

    Private smtp As String
    Private toAddress As String
    Private fromAddress As String
    Private enableOKEmail As Boolean

    Private loginPage As String
    Private mainPage As String
    Private logoutPage As String

    'Error strings and descriptions
    Private defaultError As String
    Private errors As ArrayList = New ArrayList()
    Private shortDescriptions As ArrayList = New ArrayList()
    Private longDescriptions As ArrayList = New ArrayList()
    Private logouts As ArrayList = New ArrayList()
    Private emails As ArrayList = New ArrayList()

    'Variables - locals
    Private errorState As Boolean = False
    Private loggedIn As Boolean = False

    Private lastEvent As Integer = -1
    Private lastResponse As HttpWebResponse = Nothing

    Private minimized As Boolean = False
    Private emailSent As Boolean = True
    Private loginString As String
    Private cookies As CookieCollection = New CookieCollection()

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        LoadSettings()

        If AppSettings("startMinimized") = "true" Then
            minimized = True
            Me.WindowState = FormWindowState.Minimized
            Me.Hide()
        End If

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MonitorIcon As System.Windows.Forms.NotifyIcon
    Friend WithEvents IconMenu As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents CheckTimer As System.Timers.Timer
    Friend WithEvents CheckBtn As System.Windows.Forms.Button
    Friend WithEvents MenuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents MenuExit As System.Windows.Forms.MenuItem
    Friend WithEvents resultBox As System.Windows.Forms.GroupBox
    Friend WithEvents data As System.Windows.Forms.TextBox
    Friend WithEvents url As System.Windows.Forms.TextBox
    Friend WithEvents returnCode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents timestamp As System.Windows.Forms.TextBox
    Friend WithEvents viewLogBtn As System.Windows.Forms.Button
    Friend WithEvents stateView As System.Windows.Forms.Label
    Friend WithEvents loginView As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ConfigForm))
        Me.MonitorIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.IconMenu = New System.Windows.Forms.ContextMenu()
        Me.MenuOpen = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuExit = New System.Windows.Forms.MenuItem()
        Me.CheckTimer = New System.Timers.Timer()
        Me.CheckBtn = New System.Windows.Forms.Button()
        Me.resultBox = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.returnCode = New System.Windows.Forms.TextBox()
        Me.url = New System.Windows.Forms.TextBox()
        Me.data = New System.Windows.Forms.TextBox()
        Me.timestamp = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.viewLogBtn = New System.Windows.Forms.Button()
        Me.stateView = New System.Windows.Forms.Label()
        Me.loginView = New System.Windows.Forms.Label()
        CType(Me.CheckTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.resultBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'MonitorIcon
        '
        Me.MonitorIcon.ContextMenu = Me.IconMenu
        Me.MonitorIcon.Icon = CType(resources.GetObject("MonitorIcon.Icon"), System.Drawing.Icon)
        Me.MonitorIcon.Text = "Portal Monitor: Status pending"
        Me.MonitorIcon.Visible = True
        '
        'IconMenu
        '
        Me.IconMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuOpen, Me.MenuItem3, Me.MenuExit})
        '
        'MenuOpen
        '
        Me.MenuOpen.DefaultItem = True
        Me.MenuOpen.Index = 0
        Me.MenuOpen.Text = "Open"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'MenuExit
        '
        Me.MenuExit.Index = 2
        Me.MenuExit.Text = "Exit"
        '
        'CheckTimer
        '
        Me.CheckTimer.Enabled = True
        Me.CheckTimer.Interval = 10000
        Me.CheckTimer.SynchronizingObject = Me
        '
        'CheckBtn
        '
        Me.CheckBtn.Location = New System.Drawing.Point(339, 285)
        Me.CheckBtn.Name = "CheckBtn"
        Me.CheckBtn.Size = New System.Drawing.Size(96, 23)
        Me.CheckBtn.TabIndex = 0
        Me.CheckBtn.Text = "Check now"
        '
        'resultBox
        '
        Me.resultBox.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label4, Me.Label3, Me.Label1, Me.returnCode, Me.url, Me.data, Me.timestamp, Me.Label2})
        Me.resultBox.Location = New System.Drawing.Point(8, 8)
        Me.resultBox.Name = "resultBox"
        Me.resultBox.Size = New System.Drawing.Size(427, 272)
        Me.resultBox.TabIndex = 1
        Me.resultBox.TabStop = False
        Me.resultBox.Text = "Last Status Report"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 20)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Page data:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Status code"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Page Address"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'returnCode
        '
        Me.returnCode.Location = New System.Drawing.Point(96, 64)
        Me.returnCode.Name = "returnCode"
        Me.returnCode.Size = New System.Drawing.Size(323, 20)
        Me.returnCode.TabIndex = 2
        Me.returnCode.Text = ""
        '
        'url
        '
        Me.url.Location = New System.Drawing.Point(96, 40)
        Me.url.Name = "url"
        Me.url.Size = New System.Drawing.Size(323, 20)
        Me.url.TabIndex = 1
        Me.url.Text = ""
        '
        'data
        '
        Me.data.Location = New System.Drawing.Point(10, 112)
        Me.data.Multiline = True
        Me.data.Name = "data"
        Me.data.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.data.Size = New System.Drawing.Size(408, 152)
        Me.data.TabIndex = 0
        Me.data.Text = ""
        Me.data.WordWrap = False
        '
        'timestamp
        '
        Me.timestamp.Location = New System.Drawing.Point(96, 16)
        Me.timestamp.Name = "timestamp"
        Me.timestamp.Size = New System.Drawing.Size(323, 20)
        Me.timestamp.TabIndex = 3
        Me.timestamp.Text = ""
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Timestamp"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'viewLogBtn
        '
        Me.viewLogBtn.Location = New System.Drawing.Point(236, 285)
        Me.viewLogBtn.Name = "viewLogBtn"
        Me.viewLogBtn.Size = New System.Drawing.Size(96, 23)
        Me.viewLogBtn.TabIndex = 2
        Me.viewLogBtn.Text = "View log"
        '
        'stateView
        '
        Me.stateView.ForeColor = System.Drawing.SystemColors.ControlText
        Me.stateView.Location = New System.Drawing.Point(112, 285)
        Me.stateView.Name = "stateView"
        Me.stateView.Size = New System.Drawing.Size(104, 23)
        Me.stateView.TabIndex = 3
        Me.stateView.Text = "Status: OK"
        Me.stateView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'loginView
        '
        Me.loginView.ForeColor = System.Drawing.Color.Red
        Me.loginView.Location = New System.Drawing.Point(8, 285)
        Me.loginView.Name = "loginView"
        Me.loginView.Size = New System.Drawing.Size(88, 23)
        Me.loginView.TabIndex = 4
        Me.loginView.Text = "Logged in: NO"
        Me.loginView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ConfigForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(442, 316)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.loginView, Me.stateView, Me.viewLogBtn, Me.resultBox, Me.CheckBtn})
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(100, 100)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(450, 350)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(450, 350)
        Me.Name = "ConfigForm"
        Me.ShowInTaskbar = False
        Me.Text = "Portal Monitor"
        CType(Me.CheckTimer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.resultBox.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub CheckTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles CheckTimer.Elapsed
        CheckStatus()
    End Sub

    Private Sub CheckBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBtn.Click
        CheckStatus()
    End Sub

    Private Sub MenuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuOpen.Click
        Me.Show()
        Me.Activate()
        Me.WindowState = FormWindowState.Normal
        minimized = False
    End Sub

    Private Sub ConfigForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        'prevent close if made from active window
        If Not minimized Then
            minimized = True
            Me.WindowState = FormWindowState.Minimized
            Me.Hide()
            e.Cancel = True
        End If
    End Sub

    Private Sub ConfigForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CheckStatus()
    End Sub

    Private Sub MonitorIcon_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles MonitorIcon.DoubleClick
        Me.Show()
        Me.Activate()
        Me.WindowState = FormWindowState.Normal
        minimized = False
    End Sub

    Private Sub ConfigForm_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        Logout()
    End Sub

    Private Sub MenuExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MenuExit.Click
        minimized = True
        Me.Close()
    End Sub

    Private Sub viewLogBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewLogBtn.Click
        Shell("notepad.exe " & logFile, AppWinStyle.NormalFocus)
    End Sub

    'Loads settings from the config file
    Private Sub LoadSettings()

        If loggedIn Then Logout()

        If AppSettings("emailMode") = "true" Then emailMode = True Else emailMode = False
        If AppSettings("loggingMode") = "true" Then loggingMode = True Else loggingMode = False
        If AppSettings("silentMode") = "true" Then silentMode = True Else silentMode = False

        CheckTimer.Interval = AppSettings("timerInterval") * 1000
        timeout = AppSettings("connectionTimeout") * 1000
        logFile = AppSettings("logFile")

        loginString = "realSubmit=fromButton&txtUsername=" & AppSettings("loginName") & "&txtPassword=" & AppSettings("loginPassword")
        loginPage = AppSettings("loginPage")
        logoutPage = AppSettings("logoutPage")
        mainPage = AppSettings("mainPage")

        smtp = AppSettings("smtp")
        fromAddress = AppSettings("fromAddress")
        toAddress = AppSettings("toAddress")
        If AppSettings("enableOKEmail") = "true" Then enableOKEmail = True Else enableOKEmail = False

        Dim i As Integer = 0
        Dim str As String = AppSettings("error-" & i)
        While str <> ""
            errors.Add(str)
            shortDescriptions.Add(AppSettings("shortDescription-" & i))
            longDescriptions.Add(AppSettings("longDescription-" & i))

            If AppSettings("logout-" & i) = "true" Then logouts.Add(True) Else logouts.Add(False)
            If AppSettings("sendEmail-" & i) = "true" Then emails.Add(True) Else emails.Add(False)

            i += 1
            str = AppSettings("error-" & i)
        End While

    End Sub

    'Retrieves necessary cookies and login the portal
    Function Login() As Boolean

        Dim requestLogin As HttpWebRequest = WebRequest.Create(loginPage)
        'Dim response As HttpWebResponse
        Dim data As String

        'Retrieve login page to get neccessary cookies
        requestLogin.CookieContainer = New CookieContainer()
        requestLogin.Timeout = timeout

        Try
            lastResponse = requestLogin.GetResponse()
        Catch e As WebException
            'No reply - Server probably down
            If e.Status = WebExceptionStatus.ServerProtocolViolation Then
                lastResponse = e.Response
            Else
                HandleEvent(0, True)
                Return False
            End If
        End Try

        cookies.Add(lastResponse.Cookies)
        lastResponse.Close()

        'Login
        'Post login information and get cookies - no redirect.
        Dim postLoginRequest As HttpWebRequest = WebRequest.Create(loginPage)
        postLoginRequest.AllowAutoRedirect = False
        postLoginRequest.Method = "POST"
        postLoginRequest.ContentType = "application/x-www-form-urlencoded"
        postLoginRequest.ContentLength = loginString.Length
        postLoginRequest.CookieContainer = New CookieContainer()
        postLoginRequest.CookieContainer.Add(cookies)

        Dim writer As StreamWriter = New StreamWriter(postLoginRequest.GetRequestStream())
        writer.Write(loginString)
        writer.Close()

        Try
            lastResponse = postLoginRequest.GetResponse()
        Catch e As WebException
            If e.Status = WebExceptionStatus.ProtocolError Then
                lastResponse = e.Response
            Else
                HandleEvent(0, True)
                Return False
            End If
        End Try

        lastResponse.Close()

        If Not lastResponse.Cookies("MSCSAuth") Is Nothing Then
            cookies.Add(lastResponse.Cookies)

            'Got all the cookies... :)
            'Logged in correctly
            'Pending login, cannot handle event here...
            Return True
        Else
            'Login error
            HandleEvent(1, True)
            Return False
        End If


    End Function

    'Requests the user homepage
    Private Sub CheckStatus()

        If Not loggedIn Then loggedIn = Login()
        If loggedIn Then

            Dim requestMainPage As HttpWebRequest = WebRequest.Create(mainPage)
            'Dim response As HttpWebResponse
            Dim data As String

            requestMainPage.Timeout = timeout
            requestMainPage.CookieContainer = New CookieContainer()
            requestMainPage.CookieContainer.Add(cookies)

            'Retrieve mainpage with results
            Try
                lastResponse = requestMainPage.GetResponse()
            Catch e As WebException
                'Handle protocol errors explicitly
                If e.Status = WebExceptionStatus.ProtocolError Then
                    lastResponse = e.Response
                Else
                    'Other error - server probably down
                    HandleEvent(0, True)
                    Exit Sub
                End If
            End Try

            'Update fields with results from query
            Dim reader As StreamReader = New StreamReader(lastResponse.GetResponseStream())
            Me.data.Text = reader.ReadToEnd()
            reader.Close()

            'Now we have all the neccessary data
            'Check the results for errors...

            'Page / login errors
            Dim i As Integer
            For i = 1 To errors.Count - 1
                If Me.data.Text.IndexOf(errors(i)) > -1 Then
                    HandleEvent(i, True)
                    Exit For
                End If
            Next

            'Everything okay
            If i = errors.Count Then HandleEvent(-1, False)
        End If

    End Sub

    Private Sub Logout()

        'Logout from application
        Dim requestLogout As HttpWebRequest = WebRequest.Create(logoutPage)
        Dim response As HttpWebResponse

        requestLogout.Timeout = 2000
        requestLogout.AllowAutoRedirect = False
        requestLogout.CookieContainer = New CookieContainer()
        requestLogout.CookieContainer.Add(cookies)

        'Fetch response
        Try
            response = requestLogout.GetResponse()
        Catch
            Exit Sub
        End Try

        loggedIn = False
        cookies.Add(response.Cookies)
        response.Close()

    End Sub

    'Logs events
    Private Sub HandleEvent(ByVal id As Integer, ByVal eState As Boolean)

        'Update GUI here
        If eState Then
            Me.timestamp.Text = DateTime.Now().ToLongDateString & " - " & DateTime.Now().ToLongTimeString
            Me.url.Text = mainPage
            Me.returnCode.Text = "ERROR: " & shortDescriptions(id)
            If id = 0 Then Me.data.Text = ""
            MonitorIcon.Text = timestamp.Text & ": " & shortDescriptions(id)

            stateView.Text = "Status: ERROR!"
            stateView.ForeColor = Color.Red
        Else
            'Okay
            Me.timestamp.Text = DateTime.Now().ToLongDateString & " - " & DateTime.Now().ToLongTimeString
            Me.url.Text = lastResponse.ResponseUri.AbsoluteUri
            Me.returnCode.Text = lastResponse.StatusCode & " - " & lastResponse.StatusDescription
            MonitorIcon.Text = timestamp.Text & ": Portalen er OK"

            stateView.Text = "Status: OK"
            stateView.ForeColor = Color.Black
        End If

        'Set login state
        If eState Then loggedIn = loggedIn And Not logouts(id)

        If loggedIn Then
            loginView.Text = "Logged in: Yes"
            loginView.ForeColor = Color.Black
        Else
            loginView.Text = "Logged in: NO!"
            loginView.ForeColor = Color.Red
        End If

        'Handle state changes
        If eState <> errorState Then

            'Logging
            If loggingMode Then
                Dim log As StreamWriter = New StreamWriter(logFile, True, Encoding.GetEncoding("iso-8859-1"))
                log.AutoFlush = True

                If eState Then : log.WriteLine(timestamp.Text & " :   " & shortDescriptions(id))
                Else : log.WriteLine(timestamp.Text & " :   " & "Server OK!")
                End If

                log.Close()
            End If

            'Send email
            If emailMode Then
                SmtpMail.SmtpServer = smtp

                If eState Then
                    If emails(id) Then SmtpMail.Send(fromAddress, toAddress, "Portalen svarer ikke!", shortDescriptions(id) & vbCrLf & vbCrLf & longDescriptions(id))
                ElseIf emails(lastEvent) And enableOKEmail Then
                    SmtpMail.Send(fromAddress, toAddress, "Portalen er OK", "Portalen svarer n� som den skal." & vbCrLf & "Alle systemer kj�rer som normalt.")
                End If
            End If

            'Alertbox
            If Not silentMode Then
                Dim dlg As AlertForm = New AlertForm()
                dlg.info.Text = longDescriptions(id)
                CheckTimer.Enabled = False
                dlg.ShowDialog()
                CheckTimer.Enabled = True
            End If

        End If

        errorState = eState
        lastEvent = id
    End Sub

End Class
