Public Class AlertForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents info As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(AlertForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.info = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(14, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(464, 32)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Portalen svarer ikke slik den skal!"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'info
        '
        Me.info.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.info.ForeColor = System.Drawing.Color.Red
        Me.info.Location = New System.Drawing.Point(14, 48)
        Me.info.Name = "info"
        Me.info.Size = New System.Drawing.Size(464, 64)
        Me.info.TabIndex = 1
        Me.info.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AlertForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(492, 123)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.info, Me.Label1})
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(400, 300)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(500, 150)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(500, 150)
        Me.Name = "AlertForm"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = " FEIL VED PORTALEN"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub

#End Region

End Class
